<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Student Information Form</title>
</head>
<body>

<form action="" method="post">
    Enter Student ID:
    <br>
    <input type="text" name="studentID">
    <br>
    Enter Student Name:
    <br>
    <input type="text" name="studentName">
    <br>
    Enter Acquired Mark in Bangla:
    <br>
    <input type="text" name="markBangla">
    <br>
    Enter Acquired Mark in English:
    <br>
    <input type="text" name="markEnglish">
    <br>
    Enter Acquired Mark in Math:
    <br>
    <input type="text" name="markMath">
    <br>
    <input type="submit">
</form>

<?php

if(!isset($_SESSION)) session_start();

$_SESSION['message']= "Data has been created/inserted". "<br>";

?>

<div id="message"> <?php echo $_SESSION['message']; ?> </div>

<script src="../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);

        $('#message').fadeOut (550);
        $('#message').fadeIn (550);

        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);

    })
</script>


</body>
</html>