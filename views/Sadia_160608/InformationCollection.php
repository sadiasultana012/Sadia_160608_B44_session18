<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Information Collection Form</title>
</head>
<body>

<form action="process.php" method="post">
    Enter Student's Name:
    <br>
    <input type="text" name="name">
    <br>
    Enter Student's Roll:
    <br>
    <input type="text" name="roll">
    <br>
    Mark in Bangla:
    <br>
    <input type="number" step="any" min="0" max="100" name="markBangla">
    <br>
    Mark in English:
    <br>
    <input type="number" step="any" min="0" max="100" name="markEnglish">
    <br>
    Mark in Math:
    <br>
    <input type="number" step="any" min="0" max="100" name="markMath">
    <br>
    <input type="submit">
</form>


</body>
</html>